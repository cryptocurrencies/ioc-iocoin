# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8++-dev libevent-dev libminiupnpc-dev pkg-config && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/IOCoin/DIONS.git /opt/iocoin
RUN cd /opt/iocoin/src && \
	make -f makefile.unix

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
	apt-get install -y  libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r iocoin && useradd -r -m -g iocoin iocoin
RUN mkdir /data
RUN chown iocoin:iocoin /data
COPY --from=build /opt/iocoin/src/iocoind /usr/local/bin/
USER iocoin
VOLUME /data
EXPOSE 33764 33765
CMD ["/usr/local/bin/iocoind", "-datadir=/data", "-conf=/data/iocoin.conf", "-server", "-txindex", "-printtoconsole"]